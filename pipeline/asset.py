from gazu import asset as gazu_asset

from pipeline.asset_type import AssetType


class AssetError(Exception):
    pass


class Asset(object):
    def __init__(self, entity_id=None, name=None):
        self._enitity_id = entity_id
        self._name = name

    def __new__(cls, *args, **kwargs):
        return super(Asset, cls).__new__(cls)

    def __repr__(self):
        return 'Asset({})'.format(self.id)

    def __iter__(self):
        yield 'id', self._enitity_id
        yield 'name', self.name

    def _get_asset_by_name(self):
        pass

    def _get_asset_by_id(self):
        pass

    @property
    def id(self):
        return self._enitity_id

    @property
    def name(self):
        return self._name

    @property
    def type(self):
        assert self.enitity_id is not None, 'Entity Id cannot be None'
        asset_type = gazu_asset.get_asset_type(self.enitity_id)
        return AssetType(asset_type)

    def get_task_types(self):
        pass

    def get_tasks(self):
        pass

    @property
    def project(self):
        return None

    @property
    def exists(self):
        return None

    def save(self):
        if self.exists:
            gazu_asset.update_asset(dict(self))
        else:
            gazu_asset.new_asset()

    @classmethod
    def make_from_id(cls, id):
        handle = gazu_asset.get_asset(id)
        return cls(entity_id=handle['id'], name=handle['name'])


if __name__ == '__main__':
    Asset.make_from_id('123')
