from gazu.task import all_task_types


class TaskType(object):
    def __init__(self, entity_id=None, name=None):
        self.task_type = self._get_by_entity_id(entity_id) if entity_id else self._get_by_name(name)

    def __new__(cls, *args, **kwargs):
        return super(TaskType, cls).__new__(cls)

    def __repr__(self):
        return 'TaskType({})'.format(self.id)

    def _get_by_entity_id(self, entity_id):
        return next((task_type for task_type in self.all_types() if task_type['id'] == entity_id), None)

    def _get_by_name(self, name):
        return next((task_type for task_type in self.all_types() if task_type['name'] == name), None)

    @property
    def id(self):
        return self.task_type['id']

    @property
    def name(self):
        return self.task_type['name']

    @property
    def created_at(self):
        return self.task_type['created_at']

    @property
    def priority(self):
        return int(self.task_type['priority'])

    @property
    def color(self):
        return self.task_type['color']

    @classmethod
    def all_types(cls):
        fetch_types = all_task_types()
        return fetch_types






