import getpass
import tempfile
import platform


def get_username():
    return str(getpass.getuser())

def get_user_temp_dir():
    tmp = None

    platform_system = platform.system()

    if platform_system == 'Darwin' or 'Linux' in platform_system:
        tmp = tempfile.gettempdir()
    elif platform_system == 'Windows' or platform_system == 'cygwin':
        tmp = tempfile.gettempdir().replace('\\', '/')

        username = get_username()
        tmp = tmp.split('/')
        for item in tmp:
            if '~1' in item:
                tmp[tmp.index(item)] = username
        tmp = '/'.join(tmp)

    return tmp
