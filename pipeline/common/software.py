import sys

from pipeline.logger import get_logger
logger = get_logger(__name__)


def get_application_name():
    """
    Figure out what app is running

    :return: application name, if found, else empty string
    :rtype: str
    """
    try:
        import maya.cmds as cmds
        return 'Maya'
    except ImportError:
        pass

    try:
        import nuke
        return 'Nuke'
    except ImportError:
        pass

    try:
        import c4d
        return 'C4D'
    except ImportError:
        pass

    try:
        import hiero.ui
        return 'Hiero'
    except ImportError:
        pass

    return


def check_interpreter(keyword):
    """
    Checks what kind of Python
    :param keyword:
    :return:
    """

    current_interpreter_path = sys.executable

    if keyword in current_interpreter_path:
        return True

    return False


def get_main_window():
    if check_interpreter('Maya'):
        return get_maya_main_window()

    elif check_interpreter('Nuke'):
        return get_nuke_main_window()


def get_maya_main_window():

    try:
        from PySide import QtGui
        from PySide import QtCore
        import shiboken
        import maya.OpenMayaUI as open_maya_ui

    except ImportError as e:
        logger.warning('Import failed: {}'.format(e))
        return None

    ptr_main_window = open_maya_ui.MQtUtil.mainWindow()

    if ptr_main_window:
        return shiboken.wrapInstance(long(ptr_main_window), QtGui.QMainWindow)

    return None


def get_nuke_main_window():
    try:
        from PySide import QtGui
        from PySide import QtCore

    except ImportError as e:
        logger.warning('Import failed: {}'.format(e))
        return None

    ptr_main_window = QtGui.QApplication.activeWindow()

    if ptr_main_window:
        return ptr_main_window

    return None
