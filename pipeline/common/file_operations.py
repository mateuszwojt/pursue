import os
import shutil

from pipeline.logger import get_logger

logger = get_logger(__name__)

# copyFile
def copy_file(source, dest):
    """
    Copies file from ``source`` path to ``dest`` path

    :param str source: path to the source file
    :param str dest: path to destination file
    """
    if os.path.exists(source):
        return shutil.copy2(source, dest)
    else:
        return None


def get_filename(path):
    """
    Returns file name

    :param path: path to the file
    :rtype: str
    """
    return os.path.basename(path)


def get_filename_no_ext(filename):
    """
    Returns file name without extension

    :param str filename:

    :rtype: str
    """
    return os.path.splitext(filename)[0]


def get_filename_ext(filename):
    """
    Returns file extension

    :param str filename:

    :rtype: str
    """
    return os.path.splitext(filename)[1]


# getAssetComponentName
def asset_component_name(filename, padding=None):
    """
    Extracts asset name and component name from file name

    :param str filename:
    :param int padding:
    :return:
    """
    if os.path.isfile(filename):
        fullname = get_filename_no_ext(get_filename(filename))

        try:
            asset_name = fullname.split("_")[0]
            split_name = fullname.split("_")

            master = split_name[1:][-1]
            number = split_name[1:][-1]
            master_version = split_name[1:][-2]

            if len(number) == padding and number.isdigit():
                component_name = ""
                for part in split_name[1:][:-1]:
                    component_name += part + "_"
                component_name = component_name[:-1]

            if len(number) == padding and number.isdigit() and master_version == "MASTER":
                component_name = ""
                for part in split_name[1:][:-2]:
                    component_name += part + "_"
                component_name = component_name[:-1]

            if master == "MASTER":
                component_name = ""
                for part in split_name[1:][:-1]:
                    component_name += part + "_"
                component_name = component_name[:-1]

            return asset_name, component_name

        except:
            return False


# listDirectoryFiles
def list_directory_files(path, dir_type):
    fullnames = []

    for file in os.listdir(path):
        if os.path.isfile(os.path.join(path, file)):
            if get_filename_ext(file)[1:] == dir_type:
                fullnames.append(os.path.join(path, file))

    return fullnames


# listDirectoryFolders
def list_directory_folders(path):
    return [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d))]


# createDirectory
def create_directory(path):
    if not os.path.exists(path):
        os.makedirs(path)
        return True
    else:
        logger.warning("Path {} already exists.".format(path))
        return False


def create_dummy_file(path, filename):
    """
    Creates an empty file

    :param str path:
    :param str filename:

    :return: path to newly created file
    :rtype: str
    """
    filepath = os.path.join(path, filename)
    f = open(filepath, 'w')
    f.close()

    return filepath


def delete(path):
    """
    Deletes entire directory tree forever, using ``shutil`` module

    :param str path: path to directory to be deleted

    :return: True, if directory path is deleted
    :rtype: bool
    """
    if os.path.exists(path):
        shutil.rmtree(path)  # DELETE FOREVER!
        return True
    else:
        logger.warning("Unable to delete {}".format(path))
        return False


def delete_file(path):
    if os.path.isfile(path):
        os.remove(path)
        return True
    else:
        logger.warning("Unable to delete {}".format(path))
        return False


# getFileSize
def get_file_size(path):
    """
    Returns file size in megabytes

    :param str path: path to file

    :return: if path is valid, returns size of the file formatted in megabytes, else 0
    :rtype: float
    """
    if path:
        if os.path.exists(path):
            return (os.path.getsize(path)) / (1024 * 1024.0)
        else:
            return 0
