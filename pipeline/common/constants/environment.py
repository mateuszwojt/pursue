import os


PROJECT = os.getenv('PROJECT')
SHOT = os.getenv('SHOT')
SCENE = os.getenv('SCENE')
SEQUENCE = os.getenv('SEQUENCE')
USER = os.getenv('USER')