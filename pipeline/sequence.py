from gazu.task import all_task_types_for_sequence


class Sequence(object):
    def __init__(self, entity_id=None):
        self._entity_id=entity_id

    def __new__(cls, *args, **kwargs):
        return super(Sequence, cls).__new__(cls)

    def __repr__(self):
        return 'Sequence({})'.format(self.id)

    @property
    def id(self):
        return self._entity_id

    @classmethod
    def get_tasks(cls):
        return all_task_types_for_sequence(cls.entity_id)

