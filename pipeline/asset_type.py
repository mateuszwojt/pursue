


class AssetType(object):
    def __init__(self, entity_id=None):
        self._entity_id = entity_id
        self._name = None


    def __new__(cls, *args, **kwargs):
        return super(AssetType, cls).__new__(cls)

    def __repr__(self):
        return 'AssetType({})'.format(self.id)

    def __iter__(self):
        yield 'id', self.id
        yield 'name', self.name

    @property
    def id(self):
        return self._entity_id

    @property
    def name(self):
        return self._name
