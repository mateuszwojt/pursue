import gazu


class User(object):
    def __init__(self):
        pass

    def __new__(cls, *args, **kwargs):
        return super(User, cls).__new__(cls)

    def __repr__(self):
        return 'User({})'.format(self.id)

    @property
    def name(self):
        return NotImplemented

    @property
    def fullname(self):
        return NotImplemented