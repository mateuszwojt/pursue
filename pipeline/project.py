from gazu import project as gazu_project
from gazu import asset as gazu_asset
from gazu.sorting import sort_by_name
from gazu.client import fetch_all, get

from pipeline import GazuHandle


class Project(object):
    def __init__(self, entity_id=None, name=None):
        self._entity_id = entity_id
        self._name = name

    def __new__(cls, *args, **kwargs):
        return super(Project, cls).__new__(cls)

    def __repr__(self):
        return 'Project("{}")'.format(self.name)

    def __iter__(self):
        yield 'id', self.id
        yield 'name', self.name

    def _get_project_by_id(self, entity_id):
        pass

    def _get_project_by_name(self, entity_name):
        pass

    @property
    def id(self):
        return self._entity_id

    @property
    def name(self):
        return self._name

    @property
    def description(self):
        return None

    @property
    def file_tree(self):
        return None

    @property
    def status(self):
        return

    @classmethod
    def get_all_projects(cls, open_only=False):
        out_data = []
        if open_only:
            # get projects with open status only
            fetched_projects = gazu_project.all_open_projects()
        else:
            fetched_projects = gazu_project.all_projects()

        for project in fetched_projects:
            instance = cls(project['id'])
            out_data.append(instance)
        return out_data

    def get_assets(cls, asset_type=None):
        if asset_type:
            path = "/data/projects/{project_id}/asset-types/{asset_type_id}/assets".format(
                project_id=cls.id,
                asset_type_id=asset_type.id
            )
            assets = get(path)
            return sort_by_name(assets)
        else:
            return sort_by_name(fetch_all("projects/%s/assets" % cls.id))

    @classmethod
    def make_from_id(cls, id):
        GazuHandle()
        handle = gazu_project.get_project(id)
        return cls(entity_id=handle['id'], name=handle['name'])

    @classmethod
    def make_from_name(cls, name):
        GazuHandle()
        handle = gazu_project.get_project_by_name(name)
        return cls(entity_id=handle['id'], name=handle['name'])


if __name__ == '__main__':
    show = Project.make_from_name('test')
    print show.get_assets()
