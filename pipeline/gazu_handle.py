import gazu.client

from config import config
from pipeline.logger import get_logger
from pipeline.common import constants


class GazuHandle(object):
    HOST = config.get('gazu.api.api').get('host')

    def __init__(self):
        self._logger = get_logger('GazuHandle')
        self._host = None
        self._current_user = constants.environment.USER
        self._api_version = None
        self._tokens = dict()

        self.connect()

    @property
    def host(self):
        return self._host

    @host.setter
    def host(self, url):
        global HOST
        self._host = url
        HOST = url
        gazu.client.set_host(url)

    @property
    def current_user(self):
        return self._current_user

    @current_user.setter
    def current_user(self, username):
        self._current_user = username

    @property
    def api_version(self):
        return self._api_version

    @property
    def tokens(self):
        return self._tokens

    @tokens.setter
    def tokens(self, tokens):
        self._tokens = tokens

    def connect(self):
        self.host = self.HOST
        self.login()

    def login(self):
        if self.authenticated() and bool(self.tokens):
            return

        try:
            self.tokens = gazu.log_in('admin@example.com', 'default')
        except Exception, e:
            self._logger.warning('Failed to log into gazu.')
            self._logger.error(e)

    def authenticated(self):
        auth_token = gazu.client.make_auth_header()
        if bool(auth_token):
            self._logger.debug('Session authenticated')
            return True
        self._logger.debug('Failed to authenticate session')
        return False



if __name__ == '__main__':
    handle = GazuHandle()
    print handle.tokens
    print gazu.person.all_persons()
    print gazu.project.get_project_by_name('test')
    print handle.host