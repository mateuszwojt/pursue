from Qt import QtWidgets, QtCore


class AssetSaverDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(AssetSaverDialog, self).__init__(parent)

        self.current_workfile = None

        # setup UI
        self.setup_ui()
        self.create_connections()
        self.configure_instance()

    def setup_ui(self):
        # main layout
        self.main_layout = QtWidgets.QVBoxLayout()

        # project
        self.project_lbl = QtWidgets.QLabel("Project")
        self.project_cmb = QtWidgets.QComboBox()
        self.main_layout.addWidget(self.project_lbl)
        self.main_layout.addWidget(self.project_cmb)

        # work type
        self.work_type_lbl = QtWidgets.QLabel("Work on")
        self.work_type_cmb = QtWidgets.QComboBox()
        self.main_layout.addWidget(self.work_type_lbl)
        self.main_layout.addWidget(self.work_type_cmb)

        # sequence
        self.sequence_lbl = QtWidgets.QLabel("Sequence")
        self.sequence_cmb = QtWidgets.QComboBox()
        self.main_layout.addWidget(self.sequence_lbl)
        self.main_layout.addWidget(self.sequence_cmb)

        # shot
        self.shot_lbl = QtWidgets.QLabel("Shot")
        self.shot_cmb = QtWidgets.QComboBox()
        self.main_layout.addWidget(self.shot_lbl)
        self.main_layout.addWidget(self.shot_cmb)

        # task
        self.task_lbl = QtWidgets.QLabel("Task")
        self.task_cmb = QtWidgets.QComboBox()
        self.main_layout.addWidget(self.task_lbl)
        self.main_layout.addWidget(self.task_cmb)

        # artist
        self.artist_lbl = QtWidgets.QLabel("Artist")
        self.artist_le = QtWidgets.QLineEdit()
        self.set_artist_name()
        self.main_layout.addWidget(self.artist_lbl)
        self.main_layout.addWidget(self.artist_le)

        # status
        self.status_lbl = QtWidgets.QLabel("Status")
        self.status_cmb = QtWidgets.QComboBox()
        self.main_layout.addWidget(self.status_lbl)
        self.main_layout.addWidget(self.status_cmb)

        # create thumbnail checkbox
        self.thumbnail_cb = QtWidgets.QCheckBox("Create thumbnail", self)
        self.main_layout.addWidget(self.thumbnail_cb)

        # buttons
        self.save_btn = QtWidgets.QPushButton("Save")
        self.main_layout.addWidget(self.save_btn)

    def create_connections(self):
        self.project_cmb.currentIndexChanged.connect(self.load_sequences)
        self.work_type_cmb.currentIndexChanged.connect(self.load_sequences)
        self.sequence_cmb.currentIndexChanged.connect(self.load_shots)

        self.save_btn.clicked.connect(self.save)

    def configure_instance(self):
        # disable artist label
        self.artist_lbl.setDisabled(True)

    def create_thumbnail_checked(self):
        if self.thumbnail_cb.isChecked():
            return True
        else:
            return False

    def set_artist_name(self):
        # TODO: set artist name properly
        self.artist_lbl.setText("Test")

    def create_thumbnail(self):
        thumb = viewport.grab_current_viewport()

        # TODO: save file to disk

    def save(self):
        # create file path
        filepath = ""

        # finally save file
        scene.save_file_as(filepath)


def run():
    saver_instance = AssetSaverDialog()
    saver_instance.show()

if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    dialog = AssetSaverDialog()
    dialog.show()
    sys.exit(app.exec_())
