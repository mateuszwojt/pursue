from gazu.task import get_task, get_task_by_name, get_task_by_path


class Task(object):
    def __init__(self, entity_id=None, name=None):
        self.gazu_handle = get_task_by_name(entity_id, name) if name else get_task(entity_id)

    def __new__(cls, *args, **kwargs):
        return super(Task, cls).__new__(cls)

    def __repr__(self):
        return 'Task({})'.format(self.id)

    def __iter__(self):
        #TODO: implement
        pass

    @property
    def id(self):
        return self.gazu_handle['id']









