from Qt import QtWidgets, QtCore


class AssetLoaderDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(AssetLoaderDialog, self).__init__(parent)

        # setup UI
        self.setup_ui()

        # create connections
        self.create_connections()

        # configure instance
        self.configure_instance()

    def setup_ui(self):
        # main layout
        self.main_layout = QtWidgets.QVBoxLayout()

        # toolbar
        self.toolbar = QtWidgets.QToolBar()
        self.main_layout.addWidget(self.toolbar)

        # projects combo
        self.projects_cmb = QtWidgets.QComboBox()
        self.toolbar.addWidget(self.projects_cmb)

        # work_layout
        self.work_layout = QtWidgets.QHBoxLayout()
        self.main_layout.addLayout(self.work_layout)

        # shot list
        self.shot_list = QtWidgets.QListWidget()
        self.work_layout.addWidget(self.shot_list)

        # version_list
        self.version_list = QtWidgets.QListWidget()
        self.work_layout.addWidget(self.version_list)

        # buttons
        self.load_btn = QtWidgets.QPushButton("Load file")
        self.work_layout.addWidget(self.load_btn)

        # set main layout
        self.setLayout(self.main_layout)

    def create_connections(self):
        self.projects_cmb.currentIndexChanged.connect(self.load_shots)

    def configure_instance(self):
        # resize window
        self.resize(750, 500)

        # set window flags
        # self.setWindowFlags(QtWidgets.QWindow)

        # size policy
        self.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))

    def get_selected_project(self):
        sel_project = self.projects_cmb.currentText().encode('utf-8')

        if sel_project:
            # TODO: logger
            return sel_project

        else:
            return None

    def get_selected_shot(self):
        sel_shot = self.shot_list.currentItem()

        if sel_shot:
            return sel_shot

        else:
            return None

    def get_selected_version(self):
        sel_version = self.version_list.currentItem()

        if sel_version:
            return sel_version
        else:
            return None

    def load_shots(self):
        # clear list first
        self.shot_list.clear()

        # get selected project
        sel_project = self.get_selected_project()

        # create project instance
        # TODO: create project instance from pursue-api

    def open_workfile(self):
        sel_item = self.get_selected_version()

        try:
            file = sel_item.asset.nk_file()
            # TODO: open file with method from utils module
        except:
            # TODO: logger
            return False

    def show_file_in_browser(self):
        # TODO:
        sel_version = self.get_selected_version()

        if sel_version:
            version_path = sel_version.asset.path

            # TODO: open version path in browser


class ShotAssetListItem(QtWidgets.QListWidgetItem):

    item_size = 50

    def __init__(self, item, parent=None):
        super(ShotAssetListItem, self).__init__(item.name, parent)

        self.item = item

        size_hint = self.sizeHint()
        size_hint.setHeight(self.item_size)
        self.setSizeHint(size_hint)

        font = self.font()
        font.setPixelSize(16)
        self.setFont(font)

        self.load_thumb()

    def load_thumb(self):
        # try to load thumbnail from disk first
        thumb_file = self.item.thumb_file()

        if os.path.isfile(thumb_file):
            icon = self.icon()
            icon.addFile(thumb_file)
            self.setIcon(icon)

    def update(self):
        self.setText(self.item.name)
        self.load_thumb()


def run():
    loader_instance = AssetLoaderDialog()
    loader_instance.show()

if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    dialog = AssetLoaderDialog()
    dialog.show()
    sys.exit(app.exec_())