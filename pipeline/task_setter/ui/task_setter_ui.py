from Qt import QtWidgets, QtCore


class TaskSetterDialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(TaskSetterDialog, self).__init__(parent)

        self.create_ui()

    def create_ui(self):
        self.main_layout = QtWidgets.QHBoxLayout()

        self.project_cmb = QtWidgets.QComboBox()
        self.sequence_cmb = QtWidgets.QComboBox()
        self.shot_cmb = QtWidgets.QComboBox()
        self.task_cmb = QtWidgets.QComboBox()

        self.ok_btn = QtWidgets.QPushButton('Set')
        self.cancel_btn = QtWidgets.QPushButton('Cancel')

        self.main_layout.addWidget(self.project_cmb)
        self.main_layout.addWidget(self.sequence_cmb)
        self.main_layout.addWidget(self.shot_cmb)
        self.main_layout.addWidget(QtWidgets.QLabel('Task:'))
        self.main_layout.addWidget(self.task_cmb)
        self.main_layout.addWidget(self.ok_btn)
        self.main_layout.addWidget(self.cancel_btn)

        self.setLayout(self.main_layout)

def run():
    saver_instance = TaskSetterDialog()
    saver_instance.show()

if __name__ == '__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    dialog = TaskSetterDialog()
    dialog.show()
    sys.exit(app.exec_())
