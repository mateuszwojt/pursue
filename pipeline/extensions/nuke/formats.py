import nuke

# register custom formats
nuke.addFormat("960 540 0 0 960 540 1.0 SD_540p")
nuke.addFormat("1280 720 0 0 1280 720 1.0 HD_720p")
nuke.addFormat("3840 2160 0 0 3840 2160 1.0 UHD_2160p")
nuke.addFormat("4096 2048 0 0 4096 2048 1.0 4K_DCP")
nuke.addFormat("4096 4096 0 0 4096 4096 1.0 square_4k")
nuke.addFormat("8192 8192 0 0 8192 8192 1.0 square_8k")
