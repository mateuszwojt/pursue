import nuke

from .image import ImageNode


class ReadNode(ImageNode):
    def __init__(self, read_node=None):
        assert isinstance(read_node, nuke.nodes.Read), 'Is not instance of Nuke Read node Class'

        super(ReadNode, self).__init__(read_node)

    def node(self):
        return

