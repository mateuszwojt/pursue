import nuke

nodes = ['Grade', 'ColorCorrect', 'Add', 'Gamma', 'Multiply', 'HueCorrect', 'EXPTool']

for node in nodes:
    nuke.knobDefault(node+'.unpremult', '-rgba.alpha')

# Custom default knob values
nuke.knobDefault('ColorLookup.channels', 'rgb')
nuke.knobDefault('BackdropNode.note_font_size', '128')
nuke.knobDefault('BackdropNode.note_font', 'bold')
nuke.knobDefault('BackdropNode.note_font_color', '0xffffffff')
nuke.knobDefault('Blur.label', '[value size]')
nuke.knobDefault('Blur.channels','rgba')
nuke.knobDefault('Tracker.label', '[value transform]')
nuke.knobDefault('Multiply.label', '[value value]')
nuke.knobDefault('Multiply.channels', 'rgb')
nuke.knobDefault('Add.label', '[value value]')
nuke.knobDefault('Add.channels', 'rgb')
nuke.knobDefault('Gamma.label', '[value value]')
nuke.knobDefault('Add.channels', 'rgb')
nuke.knobDefault('Gamma.channels', 'rgb')
nuke.knobDefault('Shuffle.label', '[value in] => [value out]')
nuke.knobDefault('Remove.channels', 'rgba')
nuke.knobDefault('Remove.operation', 'keep')
nuke.knobDefault('Remove.label', '[value channels]')
nuke.knobDefault('Output.note_font_size', '18')
nuke.knobDefault('Output.note_font', 'bold')
nuke.knobDefault('FrameRange.label', 'x[value knob.first_frame]-[value knob.last_frame]')
nuke.knobDefault('VectorBlur.uv','forward')
nuke.knobDefault('StickyNote.label', '<align left>')
nuke.knobDefault('Roto.cliptype', '0')
nuke.knobDefault('Merge.bbox', 'B')
nuke.knobDefault('Write.create_directories', '1')
nuke.knobDefault('Write.file_type', 'exr')
