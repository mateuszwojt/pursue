import nuke

from config import config

try:
    display_type = nuke.IMAGE | nuke.SCRIPT | nuke.GEO | nuke.FONT | nuke.PYTHON

    favorite_icon = NUKE_ICONS_PATH + r'/iconPursueMenu.png'

    favorite_dict = {
        'pursue_comp': ['pursue_comp', PIPELINE_COMP_PATH, display_type, favorite_icon],
        'pursue_2d': ['pursue_2d', PIPELINE_2D_PATH, display_type, favorite_icon],
        'pursue_hdri': ['pursue_hdri', PIPELINE_HDRI_PATH, display_type, favorite_icon],
        'pursue_render': ['pursue_render', PIPELINE_RENDER_PATH, display_type, favorite_icon],
        'pursue_assets': ['pursue_assets', PIPELINE_ASSETS_PATH, display_type, favorite_icon],
        'pursue_shots': ['pursue_shots', PIPELINE_SHOTS_PATH, display_type, favorite_icon],
        'pursue_props': ['pursue_props', PIPELINE_WORK_PROPS_PATH, display_type, favorite_icon],
        'pursue_rnd': ['pursue_rnd', PIPELINE_RND_PATH, display_type, favorite_icon]
    }

    for value_list in sorted(favorite_dict.values()):
        favorite_name, favorite_path, favorite_display_type, favorite_icon = value_list
        nuke.addFavoriteDir(favorite_name, favorite_display_type, favorite_icon)

    nuke.tprint('Successfully added pahts to favorites')

except:
    nuke.tprint('Error adding favorited to file browser')