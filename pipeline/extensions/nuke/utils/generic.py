import nuke
import nukescripts


def deselect_all():
    """
    Deselects everything inside nodegraph
    """
    # select all to invert the selection
    nuke.selectAll()
    nuke.invertSelection()


def create_backdrop(nodes, hex_color):
    """
    Creates a backdrop around given nodes, can be coloured using HEX string

    :param list[nuke.nodes] nodes: list of selected nodes
    :param int hex_color: color in hexadecimal value

    :return: Backdrop node instance
    :rtype: nuke.node.Backdrop
    """
    # deselect all nodes first
    deselect_all()

    for node in nodes:
        node.setSelected(True)

    backdrop = nukescripts.autoBackdrop()
    backdrop['tile_color'].setValue(hex_color)

    return backdrop


def rgb_to_hex(rgb_color):
    """
    Converts RGB color values to HEX string, e.g. [1.0, 0.5, 0.3] => 4286532609

    :param list[int] colorList: lif containing RGB color values

    :return: rgb color converted to HEX value
    :rtype: int
    """
    hexColor = int('%02x%02x%02x%02x' % (rgb_color[0] * 255, rgb_color[1] * 255, rgb_color[2] * 255, 1), 16)

    return hexColor


def get_layers(node, prefix):
    """
    Get layer list from multichannel EXR, can be filtered with given prefix value

    :param [nuke.nodes] node: Read node that contains layers
    :param str prefix: optional layer prefix string

    :return: list of layers found in selected Read node
    :rtype: list
    """
    # get channelList
    channels = node.channels()

    layers = []
    for channel in channels:
        layer = channel.split('.')[0]
        layers.append(layer)

    # remove duplicates
    layers = list(set(layers))

    if prefix:
        layer_list_prefix = []
        prefix_length = len(prefix)

        for layer in layers:
            if layer[0:prefix_length] == prefix:
                layer_list_prefix.append(layer)

        return layer_list_prefix

    return layers


# nodetypeMatches
def node_type_matches(node, node_type):
    """
    Checks if given node matches given type

    :param node: Node
    :param str node_type:

    :return: True, if node class equals to ``node_type`` class
    :rtype: bool
    """
    if node.Class() == node_type:
        return True

    return False


def find_nodes(start_node, node_class='Dot', operation='upstream'):
    """
    Find nodes in Nuke's nodegraph of given type, by iterating over connections.
    It can be run `upstream` or `downstream`.

    :param [nuke.nodes] start_node:
    :param str node_class:
    :param str operation:

    :return: list of matching nodes found in the graph
    """

    nodes_found = []

    def check(current_node):
        if operation == 'upstream':
            for node in current_node.dependencies():
                if node.Class() == node_class:
                    nodes_found.append(node)
                check(node)
        elif operation == 'downstream':
            for node in current_node.dependent():
                if node.Class() == node_class:
                    nodes_found.append(node)
                check(node)

    check(start_node)
    return nodes_found


def align_nodes(nodes, direction='x'):
    """Align nodes either horizontally or vertically."""

    if len(nodes) < 2:
        return
    if direction.lower() not in ('x', 'y'):
        raise ValueError('direction argument must be `x` or `y`')

    positions = [float(n[direction.lower() + 'pos'].value()) for n in nodes]
    average = sum(positions) / len(positions)
    for n in nodes:
        if direction == 'x':
            for n in nodes:
                n.setXpos(int(average))
        else:
            for n in nodes:
                n.setYpos(int(average))

    return average
