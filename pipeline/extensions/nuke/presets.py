import nuke

# custom presets
nuke.setPreset("LensDistortion", "600D 16mm", {'label': '600D 16mm', 'distortion1': '-0.02208617', 'distortion2': '0.00014866', 'distortionCenter': '0.14432046 -0.07904736'})
nuke.setPreset("LensDistortion", "600D 35mm", {'label': '600D 35mm', 'distortion1': '-0.01364814', 'distortion2': '0.00197737', 'distortionCenter': '0.27419013 -0.11746985'})
