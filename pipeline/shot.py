from gazu.task import all_task_types_for_shot


class Shot(object):
    def __init__(self, entity_id=None):
        self._entity_id=entity_id
        self._name = None

    def __new__(cls, *args, **kwargs):
        return super(Shot, cls).__new__(cls)

    def __repr__(self):
        return 'Shot({})'.format(self.id)

    def __iter__(self):
        yield 'id', self.id
        yield 'name', self.name

    @property
    def id(self):
        return self._entity_id

    @property
    def name(self):
        return self._name

    @classmethod
    def get_task_types(cls):
        return all_task_types_for_shot(cls.entity_id)
