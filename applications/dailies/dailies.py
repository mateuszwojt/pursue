import os
import re
import subprocess

from fileseq import FileSequence

from config import config
from pipeline.common.user import get_user_temp_dir
from pipeline.logger import get_logger

logger = get_logger()


class Dailies(object):
    def __init__(self):
        # Read config from yaml file
        self.presets_config = config.get('ffmpeg.presets.video_presets')
        self.ffmpeg_config = config.get('ffmpeg.general.ffmpeg')

        if os.environ.get('FFMPEG_DIR') is not None:
            ffmpeg_dir = os.environ.get('FFMPEG_DIR')
        elif self.ffmpeg_config is not None:
            ffmpeg_dir = config.get_launch_path(self.ffmpeg_config)
        else:
            exit_code = os.system('ffmpeg -version')
            if exit_code == 0:
                logger.debug("Using system global ffmpeg")
            else:
                logger.debug("Cannot find ffmpeg path")
            ffmpeg_dir = ""

        self._ffmpeg = os.path.join(ffmpeg_dir, 'ffmpeg')
        self._ffprobe = os.path.join(ffmpeg_dir, 'ffprobe')

        script_dir = os.path.dirname(os.path.realpath(__file__))
        self.res = os.path.join(script_dir, 'resources')
        self.bars = os.path.join(self.res, self.ffmpeg_config['bars'])
        self.color_bars = os.path.join(self.res, self.ffmpeg_config['color_bar'])
        self.logo = os.path.join(self.res, self.ffmpeg_config['company_logo'])
        self.logo_font_file = os.path.join(self.res, self.ffmpeg_config['company_font'])
        self.font_file = os.path.join(self.res, self.ffmpeg_config['body_font'])
        self.default_lut = os.path.join(self.res, self.ffmpeg_config['default_lut'])

        self.left_text_margin = '(w)/2+150'
        self.top_text_margin = '380'
        self.font_size = 40
        self.line_spacing = 40
        self.font_color = 'White'

        self.new_x = '1920'
        self.new_y = '1080'

        self.fields_data = {
            'company_name': None,
            'project_name': None,
            'lut': None,
            'shot_name': None,
            'file_name': None,
            'fps': None,
            'frame_range': None,
            'frame_total': None,
            'handles': None,
            'comp_res': None,
            'date': None,
            'user': None,
            'description': None,
        }

        self.tmp_files = []

    def _get_temp_dir(self):
        """
        :return: (str) Path to temporary file directory for specific platform
        """

        temp_dir = get_user_temp_dir()

        # If directory doesn't exists, create it before returning
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)

        return temp_dir

    def _get_tmp_file(self, name):
        tmp_dir = self._get_temp_dir()
        temp_file_path = os.path.join(tmp_dir, name)
        self.tmp_files.append(temp_file_path)

        return temp_file_path

    def _remove_tmp_file(self):
        for f in self.tmp_files:
            if os.path.exists(f):
                os.remove(f)

    def _get_seq(self, path):
        s = re.sub(r'%[0-9]+d', '#', path)
        seq = FileSequence.findSequencesOnDisk(os.path.dirname(s))[0]

        return seq

    def _prep_for_filter(self, path):
        """
        Prepare file path to be passed to ffmpeg complex filter
        :param path:
        :return: (str) path
        """

        # replace backslashes (win32)
        path = path.replace('\\', '/')
        # escape windows drive separator
        path = path.replace(':', '\\\\:')

        return path

    def _check_exit_status(self, status, err_msg):
        if status == 1:
            logger.error(err_msg)
            raise Exception(err_msg)

    def fields_from_dict(self, fields_dict):
        pass

    def make_slate(self, src_seq, lut=''):
        output = self._get_tmp_file('tmp_slate.png')
        seq = self._get_seq(src_seq)

        # Alignment properties for slate field
        p = "x={left_text_margin}-text_w:y={top_text_margin}+{line_spacing}".format(
            left_text_margin=self.left_text_margin,
            top_text_margin=self.top_text_margin,
            line_spacing=self.line_spacing
        )

        # Alignment properties for slate field value
        pv = "x={left_text_margin}+10:y={top_text_margin}+{line_spacing}".format(
            left_text_margin=self.left_text_margin,
            top_text_margin=self.top_text_margin,
            line_spacing=self.line_spacing
        )

        # Slate main body text
        text = "drawtext=fontsize={font_size}:fontcolor={font_color}:fontfile={font_file}:text".format(
            font_size=self.font_size,
            font_color=self.font_color,
            font_file=self._prep_for_filter(self.font_file)
        )

        if lut:
            lut_filter = (
                "[thumbnail] lut3d={lut_path} [thumbnail];"
            ).format(lut_path=self._prep_for_filter(lut))
        else:
            lut_filter = ''

        filters = (
            "[1:v] scale={new_x}:{new_y}, setsar=1:1 [base]; "
            "[0:v] scale={new_x}:{new_y} [thumbnail]; "
            "[thumbnail][3:v] overlay [thumbnail]; "
            "[thumbnail][3:v] overlay=x=(main_w-overlay_w):y=(main_h-overlay_h) [thumbnail]; "
            "[thumbnail] scale=(iw/4):(ih/4) [thumbnail]; {lut_filter}"
            "[base][thumbnail] overlay=((main_w-overlay_w)/2)-500:(main_h-overlay_h)/2 [base]; "
            "[2:v] scale=-1:-1 [self.bars]; "
            "[base][self.bars] overlay=x=(main_w-overlay_w):y=(main_h-overlay_h-50) [base]; "
            "[4:v] scale=(iw*0.2):(ih*0.2) [self.logo]; "
            "[base][self.logo] overlay=x=500:y=100 [base]; "
            "[base] "
            "drawtext=fontsize=80:fontcolor={font_color}:fontfile={logo_font_file}:text={company_name}:x=690:y=130, "
            "drawtext=fontsize=50:fontcolor={font_color}:fontfile={font_file}:text={project_name}:x=(w)/2:y=250, "
            "{text}='LUT\: ':{p}*0, "
            "{text}={lut}:{pv}*0, "
            "{text}='Shot name\: ':{p}*1, "
            "{text}={shot_name}:{pv}*1, "
            "{text}='File name\: ':{p}*2, "
            "{text}={file_name}:{pv}*2, "
            "{text}='FPS\: ':{p}*3, "
            "{text}={fps}:{pv}*3, "
            "{text}='Frame range\: ':{p}*4, "
            "{text}={frame_range}:{pv}*4, "
            "{text}='Frame total\: ':{p}*5, "
            "{text}={frame_total}:{pv}*5, "
            "{text}='Handles\: ':{p}*6, "
            "{text}={handles}:{pv}*6, "
            "{text}='Comp resolution\: ':{p}*7, "
            "{text}={comp_res}:{pv}*7, "
            "{text}='Date\: ':{p}*8, "
            "{text}={date}:{pv}*8, "
            "{text}='User\: ':{p}*9, "
            "{text}={user}:{pv}*9, "
            "{text}='Description\: ':{p}*10, "
            "{text}={description}:{pv}*10 "
        ).format(
            font_color=self.font_color, logo_font_file=self._prep_for_filter(self.logo_font_file),
            font_file=self._prep_for_filter(self.font_file), line_spacing=self.line_spacing,
            left_text_margin=self.left_text_margin,
            top_text_margin=self.top_text_margin, new_x=self.new_x,
            new_y=self.new_y, text=text, p=p, pv=pv, lut_filter=lut_filter,
            #
            # User defined fields slate values
            project_name=self.fields_data['project_name'],
            company_name=self.fields_data['company_name'],
            lut=self.fields_data['lut'],
            shot_name=self.fields_data['shot_name'],
            file_name=self.fields_data['file_name'],
            fps=self.fields_data['fps'],
            frame_range=self.fields_data['frame_range'],
            frame_total=self.fields_data['frame_total'],
            handles=self.fields_data['handles'],
            comp_res=self.fields_data['comp_res'],
            date=self.fields_data['date'],
            user=self.fields_data['user'],
            description=self.fields_data['description']
        )

        cmd = [
            self._ffmpeg, '-v', 'quiet', '-y', '-start_number', str(seq.start()), '-i', str(src_seq),
            '-f', 'lavfi', '-i', 'color=c=black', '-i', self.bars, '-i', self.color_bars,
            '-i', self.logo, '-vframes', '1', '-filter_complex', filters, str(output)
        ]

        logger.debug('Slate command : {}'.format(cmd))

        exit_status = subprocess.call(cmd)

        self._check_exit_status(exit_status, "Error occured during slate generation.")

        return output

    def make_mov(self, src_seq, out_mov, preset='', burnin=True, slate=True, lut='', quiet=False):
        # Get video presets from the config file
        video_settings = self.presets_config.get(preset)

        if video_settings is not None:
            video_settings = video_settings.split(' ')
        else:
            # Use default settings
            video_settings = [
                '-crf', '18', '-vcodec', 'mjpeg', '-pix_fmt', 'yuvj444p',
                '-qmin', '1', '-qmax', '1', '-r', '25'
            ]

        seq = self._get_seq(src_seq)

        parent, file_name = os.path.split(src_seq)

        filters = (
            "[1:v] scale={new_x}:{new_y}, setsar=1:1 [base]; "
            "[base] null "
        ).format(
            new_x=self.new_x,
            new_y=self.new_y
        )

        if lut == 'default':
            lut = self.default_lut

        if lut:
            lut_filter = (
                "[base]; [base] lut3d={lut_path}"
            ).format(
                lut_path = self._prep_for_filter(lut)
            )

            filters += lut_filter

        if slate:
            slate_filter = (
                "[base]; "
                "[0:v] trim=start_frame=0:end_frame=1 [slate]; "
                "[slate][base] concat"
            )
            filters += slate_filter

        if burnin:
            burnin_filter = (
                "[base]; [base] "
                # Left button sequence name
                "drawtext=fontsize=30: "
                "fontcolor={font_color}: "
                "fontfile={font_file}: "
                "expansion=none: "
                "text={file_name}: "
                "x=10:y=(h-(text_h+10)): "
                "enable='between(n,1,99999)', "
                # Right button frame counter
                "drawtext=fontsize=30: "
                "fontcolor={font_color}: "
                "fontfile={font_file}: "
                "start_number=1000: "
                "text=@frame_number \[{start}-{end}\]: "
                "x=(w-(text_w+10)):y=(h-(text_h+10)): "
                "enable='between(n,1,99999)'"
            ).format(
                font_color=self.font_color, font_file=self._prep_for_filter(self.font_file),
                file_name=file_name, start=seq.start(), end=seq.end()
            )
            filters += burnin_filter

        filters = filters.replace('@frame_number', '%{n}')

        cmd = [self._ffmpeg, '-v', 'quiet', '-y']

        if slate:
            slate = self.make_slate(src_seq, lut=lut)
            cmd += ['-i', slate]

        else:
            cmd += ['-f', 'lavfi', '-i', 'nullsrc=s=256x256:d=5']

        cmd += ['-start_number', str(seq.start()), '-i', src_seq]
        cmd += video_settings
        cmd += ['-filter_complex', filters, out_mov]

        if not quiet:
            # remove silent flags
            cmd.pop(1)
            cmd.pop(1)

        logger.debug('Final command : {}'.format(cmd))
        exit_status = subprocess.call(cmd)

        self._check_exit_status(exit_status, 'Error occurred during movie generation.')
        self._remove_tmp_file()

        return out_mov


if __name__ == "__main__":
    dailies = Dailies()
    dailies.make_mov(src_seq='/Users/mateuszwojt/Downloads/pursue-python/production/3d/maya/images/render/masterLayer/smoke.%04d.exr',
                     out_mov='/Users/mateuszwojt/test2.mov', burnin=True, slate=True)