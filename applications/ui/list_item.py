import os

from Qt import QtWidgets

class ListItem(QtWidgets.QListWidgetItem):
    item_size = 100

    def __init__(self, item, parent=None):
        super(ListItem, self).__init__(item, parent)
        self.item = item
        # set size hint
        size_hint = self.sizeHint()
        size_hint.setHeight(ListItem.item_size)
        self.setSizeHint(size_hint)
        # set font size
        font = self.font()
        font.setPixelSize(40)
        self.setFont(font)

    def load_thumbnail(self, thumbnail_path=None):
        if os.path.isfile(thumbnail_path):
            icon = self.icon()

