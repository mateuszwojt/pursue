# PySide
from Qt import QtWidgets


class Dialog(object):
    def __init__(self):
        self.warning_icon = PIPELINE_ICON_PATH + '/{0}.svg'.format("critical")
        self.simple_warning_icon = PIPELINE_ICON_PATH + '/{0}.svg'.format("warning")
        self.message_icon = PIPELINE_ICON_PATH + '/{0}.svg'.format("message")

    def warning(self, icon, title, message):
        if icon == "critical":
            dlg_icon = self.warning_icon
        elif icon == "warning":
            dlg_icon = self.simple_warning_icon
        else:
            dlg_icon = self.warning_icon

        reply = QtWidgets.QMessageBox()
        reply.setIconPixmap(dlg_icon)
        reply.setText(message)
        reply.setWindowTitle(title)
        reply.setStandardButtons(QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No)

        result = reply.exec_()

        if result == QtWidgets.QMessageBox.Yes:
            return True
        else:
            return False

    def message(self, icon, title, message):
        reply = QtWidgets.QMessageBox()

        if icon == "critical":
            reply.setIconPixmap(self.warning_icon)

        elif icon == "warning":
            reply.setIconPixmap(self.simple_warning_icon)

        else:
            reply.setIconPixmap(self.message_icon)

        reply.setText(message)
        reply.setWindowTitle(title)
        reply.setStandardButtons(QtWidgets.QMessageBox.Close)

        result = reply.exec_()