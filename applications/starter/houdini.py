from applications.starter.starter_item import StarterItem
from pipeline.logger import get_logger
logger = get_logger()


class HoudiniStarterItem(StarterItem):
    name = 'houdini'
    version = 'houdini_16'

    def run_houdini(self):
        cmd = self.run(self.name, self.version)
        logger.debug('Houdini launch command : {}'.format(cmd))


if __name__ == '__main__':
    HoudiniStarterItem().run_houdini()
