from applications.starter.starter_item import StarterItem
from pipeline.logger import get_logger
logger = get_logger()


class MayaStarterItem(StarterItem):
    name = 'maya'
    version = 'maya2017'

    def open_file(self, file_path=''):
        pass

    def run_maya(self):
        cmd = self.run(self.name, self.version)
        logger.debug('Maya launch command : {}'.format(cmd))


if __name__ == '__main__':
    print MayaStarterItem().name

