import subprocess
import platform

from pipeline.logger import get_logger
from config import config, environment

logger = get_logger()


class StarterItem(object):
    def __init__(self):
        self._name = ''
        self._version = ''

    @property
    def name(self):
        return self._name

    @property
    def version(self):
        return self._version

    @property
    def command(self):
        config_dict = config.get('{}.general.{}'.format(self.name, self.version))
        launch_path = config.get_launch_path(config_dict)
        return str(launch_path)

    @classmethod
    def get_environment(cls):
        env_config = environment.EnvironmentConfig()

    def open_file(self):
        pass

    def run(self):
        command = str(self.command)

        logger.debug('Command: %s' % command)

        DETACHED_PROCESS = 0x00000008
        # start maya
        if platform.system() == 'Windows':
            subprocess.Popen(command, shell=True, creationflags=DETACHED_PROCESS)
        else:
            subprocess.Popen(command, shell=True)

        return command

if __name__ == '__main__':
    StarterItem().run()