from applications.starter.starter_item import StarterItem
from pipeline.logger import get_logger
logger = get_logger()


class NatronStarterItem(StarterItem):
    @property
    def name(self):
        return 'natron'

    @property
    def version(self):
        return 'natron_2'

    def run_natron(self):
        cmd = self.run(self.name, self.version)
        logger.debug('Natron launch command : {}'.format(cmd))


if __name__ == '__main__':
    NatronStarterItem().run_natron()

