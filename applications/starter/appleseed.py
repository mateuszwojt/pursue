from applications.starter.starter_item import StarterItem
from pipeline.logger import get_logger
logger = get_logger()


class AppleseedStarterItem(StarterItem):
    @property
    def name(self):
        return 'blender'

    @property
    def version(self):
        return 'blender_278'

    def run_appleseed(self):
        cmd = self.run(self.name, self.version)
        logger.debug('Blender launch command : {}'.format(cmd))


if __name__ == '__main__':
    AppleseedStarterItem().run_appleseed()
