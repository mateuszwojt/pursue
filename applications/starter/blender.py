from applications.starter.starter_item import StarterItem
from pipeline.logger import get_logger
logger = get_logger()


class BlenderStarterItem(StarterItem):
    name = 'blender'
    version = 'blender278'

    def command(self):
        cmd = self.run(self.name, self.version)
        logger.debug('Blender launch command : {}'.format(cmd))


if __name__ == '__main__':
    BlenderStarterItem().run()
