from applications.starter.starter_item import StarterItem
from pipeline.logger import get_logger
logger = get_logger()


class GafferStarterItem(StarterItem):
    @property
    def name(self):
        return 'gaffer'

    @property
    def version(self):
        return 'gaffer_045'

    def command(self):
        cmd = self.run(self.name, self.version)
        logger.debug('Gaffer launch command : {}'.format(cmd))


if __name__ == '__main__':
    GafferStarterItem().run_gaffer()
