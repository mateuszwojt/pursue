"""
Pursue Package
==========================================

This package serves a certain functionality. This is doc line.

-----------------------

*Author* `Mateusz Wojt <mailto:mateusz3de@gmail.com>`_
*Version* 0.1
"""

import sys
import os
import functools
import logging
import subprocess
import argparse

import qdarkstyle

# PySide
from Qt import QtWidgets

from starter import StarterApp

def starter_loader():
    # Command line arguments
    parser = argparse.ArgumentParser(description = "Parse the command line for pursue_launcher variables.")
    parser.add_argument('-cyp', '--custom-yaml-path', help='Path to custom yaml file.', required=False, type=int, default=0)
    parser.add_argument('-sbx', '--sandbox', help='Use builtin sandbox yaml file.', required=False, type=int, default=0)
    parser.add_argument('-rma', '--runmaya', help='Start Maya.', required=False, type=int, default=0)
    parser.add_argument('-rnk', '--runnuke', help='Start Nuke.', required=False, type=int, default=0)
    parser.add_argument('-rho', '--runhoudini', help='Start Houdini.', required=False, type=int, default=0)
    parser.add_argument('-rmi', '--runmari', help='Start Mari.', required=False, type=int, default=0)
    parser.add_argument('-rbl', '--runblender', help='Start Blender.', required=False, type=int, default=0)
    parser.add_argument('-rgf', '--rungaffer', help='Start Gaffer.', required=False, type=int, default=0)
    parser.add_argument('-ras', '--runappleseed', help='Start Appleseed.', required=False, type=int, default=0)
    parser.add_argument('-rna', '--runnatron', help='Start Natron.', required=False, type=int, default=0)
    command_line_args_dict = vars(parser.parse_args())

    print('\n\nCommand Line Arguments\n---------------------------------------------')
    for argument_name, argument_value in command_line_args_dict.iteritems():
        print('{0} - {1}'.format(argument_name, argument_value))


    # Start pursue launcher
    app_pursue_launcher = QtWidgets.QApplication(sys.argv)
    # app_pursue_launcher.setStyleSheet(qdarkstyle.load_stylesheet(pyside=True))
    pursue_launcher_instance = StarterApp(command_line_args_dict=command_line_args_dict)

    if command_line_args_dict.get('runmaya') or command_line_args_dict.get('runnuke')\
            or command_line_args_dict.get('runhoudini') or command_line_args_dict.get('runmari')\
            or command_line_args_dict.get('runblender') or command_line_args_dict.get('rungaffer')\
            or command_line_args_dict.get('runappleseed') or command_line_args_dict.get('runnatron'):

        # launch maya
        if command_line_args_dict.get('runmaya'):
            run_maya()
        # launch nuke
        if command_line_args_dict.get('runnuke'):
            run_nuke()
        # launch houdini
        if command_line_args_dict.get('runhoudini'):
            run_houdini()
        # launch mari
        if command_line_args_dict.get('runmari'):
            run_mari()
        # launch blender
        if command_line_args_dict.get('runblender'):
            run_blender()
        # launch gaffer
        if command_line_args_dict.get('rungaffer'):
            run_gaffer()
        # launch appleseed
        if command_line_args_dict.get('runappleseed'):
            run_appleseed()
        # launch natron
        if command_line_args_dict.get('runnatron'):
            run_natron()
        # exit
        sys.exit(0)
    else:
        # show
        pursue_launcher_instance.show()
    # exit
    sys.exit(app_pursue_launcher.exec_())


# Run methods
def run_maya():
    from .maya import MayaStarterItem
    MayaStarterItem().run_maya()


def run_nuke():
    from .nuke import NukeStarterItem
    NukeStarterItem().run_nuke()


def run_houdini():
    from .houdini import HoudiniStarterItem
    HoudiniStarterItem().run_houdini()


def run_mari():
    from .mari import MariStarterItem
    MariStarterItem.run_mari()


def run_blender():
    pass


def run_gaffer():
    pass


def run_appleseed():
    pass


def run_natron():
    pass


if(__name__ == '__main__'):
    starter_loader()






