from Qt import QtWidgets, QtCore

class EnvironmentVariablesModel(QtCore.QAbstractTableModel):
    def __init__(self, parent=None):
        super(EnvironmentVariablesModel, self).__init__(parent)

        self.header_name_list = ['Env.Variable', 'Value']

        self.data_list = [[]]

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        """

        :param section:
        :param orientation:
        :param role:
        :return:
        """
        if(orientation == QtCore.Qt.Horizontal):
            if(role == QtCore.Qt.DisplayRole):
                return self.header_name_list[section]

        elif(orientation == QtCore.Qt.Vertical):
            if(role == QtCore.Qt.DisplayRole):
                return section

        return QtCore.QAbstractTableModel.headerData(self, section, orientation, role)

    def rowCount(self, parent=QtCore.QModelIndex):
        """

        :param parent:
        :return:
        """
        return len(self.data_list)

    def columnCount(self, parent=QtCore.QModelIndex):
        """

        :param parent:
        :return:
        """
        return len(self.data_list[0])

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """

        :param index:
        :param role:
        :return:
        """
        row = index.row()
        col = index.column()

        if(role == QtCore.Qt.DisplayRole):

            return self.data_list[row][col]

        else:

            return QtCore.QVariant()

    def flags(self, index):
        return QtCore.Qt.ItemIsEnabled

    def update(self, data_list):
        self.data_list = data_list
        self.reset()
