import os

from Qt import QtWidgets, QtCore


class AppLauncherButton(QtWidgets.QPushButton):
    dropped = QtCore.Signal(list)

    def __init__(self, button_text=None, icon_path=None, icon_path_hover=None, icon_path_drag=None,
                 starter_item=None, parent=None):
        """
        Abstract button for launching DCC applications inside starter app.

        :param str button_text:
        :param str icon_path:
        :param str icon_path_hover:
        :param str icon_path_drag:
        :param applications.starter.starter_item.StarterItem starter_item:
        :param parent:
        """
        if(button_text):
            super(AppLauncherButton, self).__init__(button_text, parent)
        else:
            super(AppLauncherButton, self).__init__(parent)

        # setup starter item
        self._starter_item = starter_item
        # Setup paths to the icons
        self.icon_path = icon_path

        if(self.icon_path):
            if(os.path.isfile(self.icon_path)):
                self.setStyleSheet("border-image: url({0});".format(self.icon_path))

        self.icon_path_hover = icon_path_hover
        self.icon_path_drag = icon_path_drag

        self.setAcceptDrops(True)
        self.setMouseTracking(True)

    @property
    def starter_item(self):
        return self._starter_item

    def enterEvent(self, event):
        if(self.icon_path_hover):
            if(os.path.isfile(self.icon_path_hover)):
                self.setStyleSheet("border-image: url({0});".format(self.icon_path_hover))
        event.accept()

    def leaveEvent(self, event):
        if (self.icon_path):
            if (os.path.isfile(self.icon_path)):
                self.setStyleSheet("border-image: url({0});".format(self.icon_path))
        event.accept()

    def dragEnterEvent(self, event):
        if(event.mimeData().hasUrls()):
            if (self.icon_path_drag):
                if (os.path.isfile(self.icon_path_drag)):
                    self.setStyleSheet("border-image: url({0});".format(self.icon_path_drag))
            event.accept()

        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if(event.mimeData().hasUrls()):
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dragLeaveEvent(self, event):
        if(self.icon_path):
            if(os.path.isfile(self.icon_path)):
                self.setStyleSheet("border-image: url({0});".format(self.icon_path))
        event.accept()

    def dropEvent(self, event):
        if(event.mimeData().hasUrls()):
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()

            url_list = []
            for url in event.mimeData().urls():
                url_list.append(str(url.toLocalFile()))

            self.dropped.emit(url_list)

        else:
            event.ignore()
