import os

from Qt import QtWidgets

from app_launcher_button import AppLauncherButton
from env_var_item_delegate import EnvironmentVariablesItemDelegate
from env_var_model import EnvironmentVariablesModel
from env_var_view import EnvironmentVariablesView
from app_tab_widget import AppTabWidget

ICONS_PATH = os.environ.get('ICONS_PATH', '/Users/mateuszwojt/dev/pursue/applications/icons')


class StarterMainUI(QtWidgets.QWidget):
    def __init__(self, command_line_args_dict=dict, parent=None):
        super(StarterMainUI, self).__init__(parent)

        self.title = 'StarterMainUI'
        
        self.setup_ui()
        
    def setup_ui(self):
        # setup UI
        self.main_layout = QtWidgets.QGridLayout()

        self.header_layout = QtWidgets.QHBoxLayout()
        self.wdgt_header_icon = QtWidgets.QWidget()

        self.lbl_header_text = QtWidgets.QLabel()

        self.header_layout.addWidget(self.wdgt_header_icon)
        self.header_layout.addWidget(self.lbl_header_text)

        self.main_tab_widget = QtWidgets.QTabWidget()
        self.app_tab_widget = AppTabWidget()
        self.main_tab_widget.addTab(self.app_tab_widget, 'Apps')

        self.status_layout = QtWidgets.QVBoxLayout()

        self.le_status = QtWidgets.QLineEdit()
        self.status_line1 = QtWidgets.QFrame()
        self.status_line1.setFrameShape(QtWidgets.QFrame.HLine)
        self.status_line1.setFrameShadow(QtWidgets.QFrame.Sunken)

        self.status_layout.addWidget(self.le_status)
        self.status_layout.addWidget(self.status_line1)

        # add all layouts to main_layout
        self.main_layout.addLayout(self.header_layout, 0, 0, 1, 1)
        self.main_layout.addWidget(self.main_tab_widget, 0, 0, 1, 1)
        self.main_layout.addLayout(self.status_layout, 4, 0, 1, 1)

        # set layout to main_layout
        self.setLayout(self.main_layout)

        # setup additional ui
        self.configure_instance()

    def configure_instance(self):
        header_icon_name = 'icon_pursue_launcher.png'
        header_icon_path = os.path.join(ICONS_PATH, header_icon_name)

        # set size policy
        self.setSizePolicy(QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding))
        self.resize(491, 737)
        self.wdgt_header_icon.setStyleSheet("border-image: url({0});".format(header_icon_path.replace('\\', '/')))
        self.setWindowTitle(self.title)

        self.lbl_header_text.setText(self.title)

        # setup dcc buttons
        self.setup_mvc()

    def setup_mvc(self):
        self.env_var_table_view = EnvironmentVariablesView()
        self.env_var_table_view_item_delegate = EnvironmentVariablesItemDelegate()
        self.env_var_table_view.setItemDelegate(self.env_var_table_view_item_delegate)
        self.env_var_table_model = EnvironmentVariablesModel()
        self.env_var_table_view.setModel(self.env_var_table_model)


