from Qt import QtWidgets, QtCore, QtGui


class EnvironmentVariablesItemDelegate(QtWidgets.QStyledItemDelegate):
    def __init__(self, parent=None):
        super(EnvironmentVariablesItemDelegate, self).__init__(parent)

    def sizeHint(self, option=QtWidgets.QStyleOptionViewItem, index=QtCore.QModelIndex):
        """

        :param option:
        :param index:
        :return:
        """
        if index.isValid():
            data = index.data(QtCore.Qt.DisplayRole)

            row = index.row()
            column = index.column()

            if type(data) is list:
                value_string = ''
                for index, value in enumerate(data):
                    if index == len(data) - 1:
                        value_string += str(value)
                        continue

                    value_string += str(value + ';\n')

                font_metrics = QtGui.QFontMetrics(QtWidgets.QApplication.font())
                text_size = font_metrics.size(0, value_string)
                return text_size

            else:
                return super(EnvironmentVariablesItemDelegate, self).sizeHint(option, index)

        else:
            return super(EnvironmentVariablesItemDelegate, self).sizeHint(option, index)

    def paint(self, painter=QtGui.QPainter, option=QtWidgets.QStyleOptionViewItem, index=QtCore.QModelIndex):
        """
        
        :param painter:
        :param option:
        :param index:
        :return:
        """
        if index.isValid():
            data = index.data(QtCore.Qt.DisplayRole)
            painter.save()

            if type(data) is list:
                value_string = ''
                for index, value in enumerate(data):
                    if index == len(data) - 1:
                        value_string += str(value)
                        continue

                    value_string += str(value + ';\n')

                painter.drawText(option.rect, QtCore.Qt.AlignLeft, value_string)

            else:
                super(EnvironmentVariablesItemDelegate, self).paint(painter, option, index)

            painter.restore()

        else:
            super(EnvironmentVariablesItemDelegate, self).paint(painter, option, index)