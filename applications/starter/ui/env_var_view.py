from Qt import QtWidgets


class EnvironmentVariablesView(QtWidgets.QTableView):
    def __init__(self, parent=None):
        super(EnvironmentVariablesView, self).__init__(parent)

        self.setWordWrap(True)
        self.horizontalHeader().setResizeContentsPrecision(3)
        self.horizontalHeader().setStretchLastSection(True)
        self.verticalHeader().setResizeContentsPrecision(3)
        self.setAlternatingRowColors(True)