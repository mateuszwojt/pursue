import os

from Qt import QtWidgets, QtCore

from app_launcher_button import AppLauncherButton

# starter modules
from applications.starter.maya import MayaStarterItem
from applications.starter.nuke import NukeStarterItem
from applications.starter.houdini import HoudiniStarterItem
from applications.starter.mari import MariStarterItem
from applications.starter.blender import BlenderStarterItem
from applications.starter.gaffer import GafferStarterItem
from applications.starter.appleseed import AppleseedStarterItem
from applications.starter.natron import NatronStarterItem

APP_STARTER_ITEMS = [
    MayaStarterItem,
    NukeStarterItem,
    HoudiniStarterItem,
    MariStarterItem,
    BlenderStarterItem,
    GafferStarterItem,
    AppleseedStarterItem,
    NatronStarterItem
]

ICONS_PATH = os.environ.get('ICONS_PATH', '/Users/mateuszwojt/dev/pursue/applications/icons')


class AppTabWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(AppTabWidget, self).__init__(parent)

        self.apps = [
            'maya',
            'houdini',
            'nuke',
            'mari'
        ]
        self.dcc_buttons = []

        container = QtWidgets.QWidget()

        self.dcc_buttons_layout = QtWidgets.QVBoxLayout(self)
        self.dcc_buttons_layout.setContentsMargins(5, 5, 5, 5)
        self.dcc_buttons_layout.setSpacing(5)
        self.setup_dcc_buttons()
        container.setLayout(self.dcc_buttons_layout)

        scroll_area = QtWidgets.QScrollArea()
        scroll_area.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        scroll_area.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        scroll_area.setWidgetResizable(False)
        scroll_area.setWidget(container)

        main_layout = QtWidgets.QVBoxLayout(self)
        main_layout.addWidget(scroll_area)
        self.setLayout(main_layout)

    @staticmethod
    def icon_paths(dcc_name=''):
        button_icon_path = os.path.join(ICONS_PATH, 'icon_dcc_button_{0}.png'.format(dcc_name))
        button_icon_path_hover = os.path.join(ICONS_PATH, 'icon_dcc_button_{0}_hover.png'.format(dcc_name))
        button_icon_path_drag = os.path.join(ICONS_PATH, 'icon_dcc_button_{0}_drag.png'.format(dcc_name))

        return button_icon_path, button_icon_path_hover, button_icon_path_drag

    def setup_dcc_buttons(self):
        for app in self.apps:
            button_icon_path, button_icon_path_hover, button_icon_path_drag = self.icon_paths(dcc_name=app)
            btn_launch_dcc = AppLauncherButton(
                button_text='Launch {0}'.format(app),
                icon_path=button_icon_path,
                icon_path_hover=button_icon_path_hover,
                icon_path_drag=button_icon_path_drag,
                starter_item=next((item for item in APP_STARTER_ITEMS if item.name == app), None)
            )
            btn_launch_dcc.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
            self.dcc_buttons.append(btn_launch_dcc)
            self.dcc_buttons_layout.addWidget(btn_launch_dcc)

if __name__ == '__main__':
    apps = [
        'maya',
        'houdini',
        'nuke',
        'mari'
    ]
    for app in apps:
        print 'app: %s' % app
        starter_item = next((item for item in APP_STARTER_ITEMS if item.name == app), None)
        # starter_item = filter(lambda item: item.name == app, APP_STARTER_ITEMS)
        print 'starter_item: %s' % starter_item

