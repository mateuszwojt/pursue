import os
import sys
import functools
import qdarkstyle

from Qt import QtWidgets

from pipeline.logger import get_logger


from applications.starter.ui.starter_main_ui import StarterMainUI

ICONS_PATH = os.environ.get('ICONS_PATH', '/Users/mateuszwojt/dev/pursue/applications/icons')
logger = get_logger(__name__)

class StarterApp(StarterMainUI):
    def __init__(self, command_line_args_dict=dict, parent=None):
        super(StarterApp, self).__init__(parent)

        self.command_line_args_dict = command_line_args_dict

        self.env_var_dict = {}
        self.env_var_list = []

        # logger
        self.logger = get_logger()

        # connect_ui
        self.create_connections()

    def create_connections(self):
        for dcc_button in self.app_tab_widget.dcc_buttons:
            logger.debug('Adding button for %s' % dcc_button.starter_item.name)
            dcc_button.clicked.connect(functools.partial(dcc_button.starter_item.run))
            dcc_button.dropped.connect(functools.partial(dcc_button.starter_item.open_file))

def run():
    app = QtWidgets.QApplication(sys.argv)
    app.setStyleSheet(qdarkstyle.load_stylesheet(pyside=True))

    pursue_launcher_instance = StarterApp()
    pursue_launcher_instance.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    run()
