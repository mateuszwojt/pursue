from applications.starter.starter_item import StarterItem
from pipeline.logger import get_logger
logger = get_logger()


class NukeStarterItem(StarterItem):
    name = 'nuke'
    version = 'nuke100'

    def run_nuke(self):
        cmd = self.run()
        logger.debug('Nuke launch command : {}'.format(cmd))


if __name__ == '__main__':
    print NukeStarterItem().name
