from applications.starter.starter_item import StarterItem
from pipeline.logger import get_logger
logger = get_logger()


class MariStarterItem(StarterItem):
    name = 'mari'
    version = 'mari_4'

    def run_mari(self):
        cmd = self.run(self.name, self.version)
        logger.debug('Mari launch command : {}'.format(cmd))


if __name__ == '__main__':
    MariStarterItem().run_mari()

