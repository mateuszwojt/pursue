import os
import yaml
import platform

CONFIG_ROOT = os.path.dirname(os.path.abspath(__file__))


def get(config_path):
    """
    Resolves a given path to point a application config yaml inside `config` directory,
    returns it's value

    :param str config_path:
    :return: dictionary containing config values
    :rtype: dict
    """
    resolved_path = config_path.split('.')
    config_file = os.path.join(CONFIG_ROOT, resolved_path[0], resolved_path[1] + '.yml')

    with open(config_file, 'r') as f:
        cfg = yaml.load(f)

    value = cfg.get(resolved_path[2])

    if value is None:
        raise ValueError('Config value : {} doesnt exist!'.format(resolved_path[3]))
    else:
        if isinstance(value, list):
            return value
        if len(resolved_path) > 3:
            item = next((obj[1] for obj in value.iteritems() if obj[0] == resolved_path[3]), None)
            return item
        else:
            return value


def get_launch_path(config_dict):
    platform_system = platform.system()

    if platform_system == 'Windows':
        return config_dict['windows_launch_path']
    elif platform_system == 'Darwin':
        return config_dict['darwin_launch_path']
    elif platform_system == 'Linux':
        return config_dict['linux_launch_path']
    else:
        raise AttributeError('No matching system found for launch_path config entry.')

# test = get('nuke.general.nuke_105.file_ext')
# print test
# for i, t in enumerate(test.itervalues()):
#     print i, t, i % 2
# config_file = os.path.join(CONFIG_ROOT, 'starter', 'applications.yml')
# with open(config_file, 'r') as f:
#     cfg = yaml.load(f)
#
# print cfg['apps']
