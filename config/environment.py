import os
import re
import yaml
from collections import deque

from pipeline.logger import get_logger

logger = get_logger()


class EnvironmentConfig(object):
    def __init__(self):
        yaml.add_constructor(u'!omap', self.omap_constructor)
        self.config = {}

    def __getitem__(self, item):
        return self.config[item]

    def keys(self):
        return self.config.keys()

    def omap_constructor(self, loader, node):
        return loader.construct_pairs(node)

    @staticmethod
    def _load_file(config_file):
        try:
            with open(str(config_file), 'r') as f:
                config = yaml.load(f)
                return config
        except IOError:
            return

    def from_file(self, config_file, dry_run=False):
        """
        Read configuration from an YAML file

        :param str config_file: Path to config file
        :param bool dry_run: if True, loads config file without setting environment
        """

        self.config = self._load_file(config_file)

        if not self.config:
            raise Exception('Cannot load and set environment from {}'.format(config_file))

        if dry_run:
            return

        self.set_env_vars()

    def set_env_vars(self):
        """
        Parse and set all environmental variables
        """
        logger.line()

        for k, v in self.config.items():
            for i in v:
                name = str(i[0])
                values = i[1]

                if not isinstance(values, list):
                    values = [values]

                queue = deque()
                for value in values:
                    value = str(value)

                    # Get template value
                    template_values = re.findall('<(.*?)>', value)

                    # This will try to set all of the template values to
                    # their corresponding environmental variable
                    for v in template_values:
                        try:
                            v_val = os.environ[v]
                        except KeyError:
                            v_val = ''
                        value = value.replace('<%s>' % v, v_val)

                    queue.append(value)

                # Assemble new path for current env var
                path = ''
                cout = 0
                while queue:
                    if cout == 0:
                        path = queue.popleft()
                    else:
                        path = path + os.pathsep + queue.popleft()
                    cout += 1

                # Assign path to current variable
                logger.info('%s %s' % (name, path))
                os.environ[name] = path
        logger.line()

if __name__ == '__main__':
    ev = EnvironmentConfig()
    ev.from_file('/Users/mateuszwojt/dev/pursue/config/global/paths.yml', dry_run=True)